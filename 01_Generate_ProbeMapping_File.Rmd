---
title: "Generate gene ID mapping files"
author: "Matthew Roberts"
date: "20/12/2021"
output: html_document
---

<style type="text/css">
.main-container {
  max-width: 90%;
  margin-left: auto;
  margin-right: auto;
}
</style>

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Introduction

Downstream analyses of differentially expressed genes require specific gene IDs. The differentially expressed gene analysis results are reported by microarray probe ID (GSE33479 and GSE114489), gene symbol (GSE108124) and Ensembl ID (GSE109743). It is ideal to have multiple gene IDs (Ensembl ID, gene symbol and Entrez ID) for a gene to ensure differentially expressed genes can be used in a variety of enrichment analysis tools. 

## Libraries

```{r message=FALSE, warning=FALSE, echo=T}
library(magrittr)
library(dplyr)
library(GEOquery)
library(rstudioapi)
library(limma)
library(AnnotationDbi)
library(org.Hs.eg.db)
```

## Setting the work directory

Let's change the working directory.

```{r results='hide', message=FALSE, warning=FALSE, echo=T}
# Getting the path of your current open file
current_path = rstudioapi::getActiveDocumentContext()$path 
setwd(dirname(current_path ))
print( getwd() )
rm(list=ls())
```

## Retrieving Data

The goal is to create a data.frame object containing the probe IDs and associated gene symbols, Ensembl IDs and Entrez IDs that will help with finding specific genes by more user-friendly gene symbols and performing downstream analyses. The data for GSE33479 contains probe IDs, gene symbols and Ensembl transcript IDs. We'll start by retrieving Ensembl gene IDs using each of these three IDs. 

The platform data can be found here: <https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GPL6480>.
Which is the platform for this dataset: <https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE33479>.
But it's easier to just generate the results and grab the IDs from the results object.

The data took some time to download and sometimes the data didn't finish downloading under default settings. Let's increase the time allocated for fetching data:

```{r results='hide', message=FALSE, warning=FALSE, echo=T}
getOption('timeout') # default is 60
options(timeout=600)
```

A tutorial for analysing such data was found here: <https://wiki.bits.vib.be/index.php/Analyse_GEO2R_data_with_R_and_Bioconductor>. A script with similar code is also generated if you analyse GEO data using GEO2R online tool.

```{r results='hide', message=FALSE, warning=FALSE, echo=T}
gset <- GEOquery::getGEO("GSE33479", GSEMatrix =TRUE)
if (length(gset) > 1) {idx <- grep("GPL6480", attr(gset, "names"))} else {idx <- 1}    
gset <- gset[[idx]]
saveRDS(gset, "Files_data/gset_GSE33479.rds")
fvarLabels(gset) <- make.names(fvarLabels(gset))
# This dataset has 9 groups ranging from normal normofluorescent (0) to squamous cell carcinoma (8)
# Each digit represents a replicate belonging to that group.
gsms <- paste0("0000000000000",
               "11111111111111",
               "222222222222222",
               "333333333333333",
               "4444444444444",
               "5555555555555",
               "666666666666",
               "7777777777777",
               "88888888888888")
sml <- c()
for (i in 1:nchar(gsms)) { sml[i] <- substr(gsms,i,i) }
rm(gsms,i)
sml <- paste("G", sml, sep="")
fl <- as.factor(sml)
gset$description <- fl

design <- model.matrix(~ description + 0, gset)
colnames(design) <- levels(gset$description)
fit <- lmFit(gset, design)
cont.matrix <- makeContrasts("G0-G8", levels=design) # Contrast doesn't matter, we just want the results
fit2 <- contrasts.fit(fit, cont.matrix)
fit2 <- eBayes(fit2, 0.01)
tT <- topTable(fit2, adjust="fdr", sort.by="B", number=nrow(fit2)) # Results for all probes
results <- as.data.frame(tT)
results <- results %>% dplyr::select(c("ID", "GENE_SYMBOL", "ENSEMBL_ID")) # Probe ID, gene symbol and transcript ID but we will need Ensembl Gene IDs for some downstream analysis tools
rm(cont.matrix, design, fit, fit2, gset, tT, fl, idx, sml)
```

## Ensembl BioMart files

Retrieving gene IDs using the biomaRt R package is an option but can be slow. Therefore data files were obtained from the Ensembl BioMart website: <https://www.ensembl.org/biomart/martview/>. <br>
Database: Ensembl Genes 104; Human Genes GRCh38.p13 <br>

And the following attributes were selected to create four separate files: <br>
Ensembl_BioMart_AgilentWholeGenome_4x44_v1_2021_May_104.txt: 'Gene stable ID' and 'AGILENT WholeGenome 4x44k v1 probe' <br>
Ensembl_BioMart_Transcript_2021_May_104.txt: 'Gene stable ID' and 'Transcript stable ID' <br>
Ensembl_BioMart_GeneName_2021_May_104.txt: 'Gene stable ID' and 'Gene name' <br>
Ensembl_BioMart_EntrezID_2021_May_104.txt: 'Gene stable ID' and 'NCBI gene (formerly Entrezgene) ID' <br>

Let's begin by tidying up the Ensembl ID file that contains Ensembl IDs and probe IDs:

```{r results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_Probe <- read.csv("Files_Ensembl/Ensembl_BioMart_AgilentWholeGenome_4x44_v1_2021_May_104.txt", stringsAsFactors = F)
```
We have Ensembl IDs with no probe IDs:
```{r message=FALSE, warning=FALSE, echo=T}
head(Ensembl_Probe[ which(Ensembl_Probe$AGILENT.WholeGenome.4x44k.v1.probe == "") ,])
```
Let's remove these:

```{r results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_Probe <- Ensembl_Probe[ -which(Ensembl_Probe$AGILENT.WholeGenome.4x44k.v1.probe == ""), ]
```

Let's check for duplicates:

```{r message=FALSE, warning=FALSE, echo=T}
Ensembl_Probe <- Ensembl_Probe %>% dplyr::mutate(Check = paste(Ensembl_Probe$Gene.stable.ID, Ensembl_Probe$AGILENT.WholeGenome.4x44k.v1.probe, sep = "_"))
head( Ensembl_Probe[ which(Ensembl_Probe$Check %in% Ensembl_Probe$Check[which(duplicated(Ensembl_Probe$Check))]) , ] )
```

We have duplicates so let's remove them:

```{r results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_Probe <- Ensembl_Probe[ -which(duplicated(Ensembl_Probe$Check)==T) , ] %>% dplyr::select(-c(Check))
```

Let's check that we have one Ensembl ID for one probe ID and if not, we need to resolve these multimapping cases. We want to resolve these cases because we want one ID for one probe when it comes to enrichment analysis. We'll resolve these cases by grouping by probe ID and then collating Ensembl IDs. We can separate IDs by commas and then search for any commas in our Ensembl ID column. We have already accounted for missing probe IDs and duplicate entries so any multimapping cases should involve distinct Ensembl IDs:

```{r results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_Probe <- Ensembl_Probe %>%  
  dplyr::group_by(AGILENT.WholeGenome.4x44k.v1.probe) %>% 
  dplyr::summarise(Gene.stable.ID = paste(Gene.stable.ID, collapse = ",")) %>% 
  ungroup()

idx <- which(grepl(",", Ensembl_Probe$Gene.stable.ID))
```

There are probes with multiple Ensembl IDs:

```{r  message=FALSE, warning=FALSE, echo=T}
head(Ensembl_Probe[idx,])
```

These multimapping cases will be removed and resolved separately before being merged with the probe IDs that only had one Ensembl ID to begin with.

```{r  message=FALSE, warning=FALSE, echo=T}
Ensembl_Probe.Multimapping <- Ensembl_Probe[idx,]
Ensembl_Probe <- Ensembl_Probe[-idx,]
rm(idx)
```

We will resolve multimapping by: <br>
  1. Separate Ensembl IDs so each row corresponds to one Ensembl ID <br>
  2. Rertrieve the gene symbol for each Ensembl ID using data from org.Hs.eg.db and function mapIds from AnnotationDbi <br>
  3. Filter out any rows without a gene symbol <br>
  4. Group by probe ID <br>
  5. Check if gene symbols are in fact the same <br>
  6. Remove any entries where gene symbols are not the same <br>
  7. Retrieve the Ensembl ID but this time using the gene symbol <br>
  8. Keep the probe ID and this new Ensembl ID <br>

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_Probe.Unimapping <- Ensembl_Probe.Multimapping %>%
  tidyr::separate_rows(Gene.stable.ID, sep = ",") %>%
  dplyr::mutate(GeneSymbol = mapIds(org.Hs.eg.db, 
                                    keys=Gene.stable.ID, 
                                    column="SYMBOL", 
                                    keytype="ENSEMBL", 
                                    multiVals="first") ) %>%
  dplyr::filter(!is.na(GeneSymbol)) %>%
  dplyr::group_by(AGILENT.WholeGenome.4x44k.v1.probe) %>%
  dplyr::mutate(GeneSymbolUnique = ifelse( length(unique(GeneSymbol))==1, GeneSymbol[1], NA ) ) %>% 
  ungroup() %>%
  dplyr::filter(!is.na(GeneSymbolUnique)) %>%
  dplyr::mutate(Gene.stable.ID.New = mapIds(org.Hs.eg.db, 
                                            keys=GeneSymbolUnique, 
                                            column="ENSEMBL", 
                                            keytype="SYMBOL", 
                                            multiVals="first") ) %>%
  dplyr::select(AGILENT.WholeGenome.4x44k.v1.probe, Gene.stable.ID.New)
```

And now to merge the probes that were removed due to having multiple Ensembl IDs but now have a single Ensembl ID

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
colnames(Ensembl_Probe.Unimapping)[which(colnames(Ensembl_Probe.Unimapping)=="Gene.stable.ID.New")] <- "Gene.stable.ID"
Ensembl_Probe.Unimapping <- Ensembl_Probe.Unimapping[-which(duplicated(Ensembl_Probe.Unimapping$AGILENT.WholeGenome.4x44k.v1.probe)),]
Ensembl_Probe <- rbind(Ensembl_Probe, Ensembl_Probe.Unimapping)
rm(Ensembl_Probe.Unimapping, Ensembl_Probe.Multimapping)
```

File containing Ensembl gene IDs and Ensembl transcript IDs:

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_Transcript <- read.csv("Files_Ensembl/Ensembl_BioMart_Transcript_2021_May_104.txt", stringsAsFactors = F)
```

File containing Ensembl gene IDs and gene symbols:

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_GeneName <- read.csv("Files_Ensembl/Ensembl_BioMart_GeneName_2021_May_104.txt", stringsAsFactors = F)
```

And now that we are aware of AnnotationDbi and org.Hs.eg.db, let's also retrieve symbols/names from org.Hs.eg.db. org.Hs.eg.db has some gene names for Ensembl IDs that BioMart does not have gene names for. However, by manual inspection (GeneCards and viewing MSigDB collections), if there is a conflict, we may have better results if we keep the gene symbol from BioMart for downstream analyses. So we have Ensembl IDs and Gene names (Gene.name) from BioMart. We retrieve gene names from org.Hs.eg.db (GeneSymbol). If we have a missing gene name from BioMart (Gene.name), we grab the name from GeneSymbol column. Otherwise we use BioMart names even if there is a different name from org.Hs.eg.db. And then we remove any entries with missing gene names.

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_GeneName <- Ensembl_GeneName %>%
  dplyr::mutate(GeneSymbol = mapIds(org.Hs.eg.db, keys=Gene.stable.ID, column="SYMBOL", keytype="ENSEMBL", multiVals="first") ) %>%
  dplyr::mutate(Gene.name = ifelse( Gene.name=="", GeneSymbol, Gene.name ) ) %>%
  dplyr::select(-GeneSymbol)
Ensembl_GeneName <- Ensembl_GeneName[ -which(is.na(Ensembl_GeneName$Gene.name)), ]
```

We need to check that one gene symbol has just one Ensembl ID:

```{r message=FALSE, warning=FALSE, echo=T}
Duplicated.GeneNames <- Ensembl_GeneName$Gene.name[which(duplicated(Ensembl_GeneName$Gene.name)==T)]
head(Duplicated.GeneNames)
```

We will once again remove these duplicate cases, resolve them separately and then merge with the existing data.frame

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_GeneName <- Ensembl_GeneName[ -which( Ensembl_GeneName$Gene.name %in% Duplicated.GeneNames ) ,]
Duplicated.GeneNames <- data.frame(Gene.name = unique(Duplicated.GeneNames))
Duplicated.GeneNames <- Duplicated.GeneNames %>% 
  dplyr::mutate(Gene.stable.ID = mapIds(org.Hs.eg.db, keys=Gene.name, column="ENSEMBL", keytype="SYMBOL", multiVals="first"))

length(which( Ensembl_GeneName$Gene.name %in% Duplicated.GeneNames ))
head(Ensembl_GeneName[ which( Ensembl_GeneName$Gene.name %in% Duplicated.GeneNames ) ,])
```

We have about ~300 genes that don't have an Ensembl Gene ID using this method but there were multiple Ensembl IDs associated with the gene name from BioMart. But since it's not clear how to pick a single Ensembl ID, these genes will just be removed. This is because it might be worse to use multiple IDs for the same gene for enrichment analyses.

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
Duplicated.GeneNames <- Duplicated.GeneNames[ -which(is.na(Duplicated.GeneNames$Gene.stable.ID)) ,]
Ensembl_GeneName <- Ensembl_GeneName %>% rbind(Duplicated.GeneNames) # just 3 duplicated Ensembl IDs
rm(Duplicated.GeneNames)
```

Finally, read in the file from BioMart with Ensembl IDs and Entrez IDs. Then add another column for Entrez IDs obtained from org.Hs.eg.db. If the Ensembl ID from BioMart is missing, grab the ID from org.Hs.eg.db. If the Entrez IDs do not match, this time we'll keep the Entrez IDs from org.Hs.eg.db.

```{r message=FALSE, warning=FALSE, echo=T}
Ensembl_Entrez <- read.csv("Files_Ensembl/Ensembl_BioMart_EntrezID_2021_May_104.txt", stringsAsFactors = F)
colnames(Ensembl_Entrez)[ which(colnames(Ensembl_Entrez) == "NCBI.gene..formerly.Entrezgene..ID") ] <- "EntrezID"
Ensembl_Entrez <- Ensembl_Entrez %>% 
  dplyr::mutate(Entrez = mapIds(org.Hs.eg.db, keys=Gene.stable.ID, column="ENTREZID", keytype="ENSEMBL", multiVals="first") )

head( Ensembl_Entrez[ which( !is.na(Ensembl_Entrez$EntrezID) & Ensembl_Entrez$EntrezID != Ensembl_Entrez$Entrez ) ,] )

```

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
Ensembl_Entrez <- Ensembl_Entrez %>%
  dplyr::mutate(EntrezID = ifelse( is.na(EntrezID), Entrez, EntrezID ) ) %>%
  dplyr::mutate(EntrezID = ifelse( !is.na(EntrezID) & EntrezID != Entrez, Entrez, EntrezID ) ) %>%
  dplyr::select(-Entrez)
Ensembl_Entrez <- Ensembl_Entrez[-which(duplicated(Ensembl_Entrez$Gene.stable.ID)),]
Ensembl_Entrez <- Ensembl_Entrez[-which(is.na(Ensembl_Entrez$EntrezID)),]
```

## Merge

Let's combine the data.frame containing the probe IDs, gene symbols and transcript IDs with the three ID files we obtained using each of these IDs to retrieve Ensembl IDs for.

```{r  message=FALSE, warning=FALSE, echo=T}
results <- results %>% merge(Ensembl_GeneName, by.x = "GENE_SYMBOL", by.y = "Gene.name", all.x = T)
colnames(results)[which(colnames(results)=="Gene.stable.ID")] <- "Gene.stable.ID.by.GENE_SYMBOL"
results <- results %>% merge(Ensembl_Transcript, by.x = "ENSEMBL_ID", by.y = "Transcript.stable.ID", all.x = T)
colnames(results)[which(colnames(results)=="Gene.stable.ID")] <- "Gene.stable.ID.by.Transcript.ID"
results <- results %>% merge(Ensembl_Probe, by.x = "ID", by.y = "AGILENT.WholeGenome.4x44k.v1.probe", all.x = T)
colnames(results)[which(colnames(results)=="Gene.stable.ID")] <- "Gene.stable.ID.by.Probe.ID"

print(paste0("Number of Ensembl IDs using gene symbol: ", length( which( !is.na( results$Gene.stable.ID.by.GENE_SYMBOL ) ) ) ))
print(paste0("Number of Ensembl IDs using transcript ID: ", length( which( !is.na( results$Gene.stable.ID.by.Transcript.ID ) ) ) ))
print(paste0("Number of Ensembl IDs using probe ID: ", length( which( !is.na( results$Gene.stable.ID.by.Probe.ID ) ) ) ))

print(paste0("Number of probe IDs with gene symbols but missing transcript IDs: ", length( which( results$GENE_SYMBOL!="" & results$ENSEMBL_ID=="" ) ) ))
print(paste0("Number of probe IDs with transcript IDs but missing gene symbols: ", length( which( results$GENE_SYMBOL=="" & results$ENSEMBL_ID!="" ) ) ))

```

## Ensembl ID

For the Ensembl ID, after manual inspection, we will prioritise the Ensembl ID found using gene symbols, then probe ID and finally we'll use transcript ID to fill in any missing Ensembl IDs.

Some examples of where Ensembl ID found using gene symbols are not the same as Ensembl IDs found using probe IDs:
```{r  message=FALSE, warning=FALSE, echo=T}
head( results[ which(!is.na(results$Gene.stable.ID.by.GENE_SYMBOL) & !is.na(results$Gene.stable.ID.by.Probe.ID) & results$Gene.stable.ID.by.GENE_SYMBOL!=results$Gene.stable.ID.by.Probe.ID) ,] ,20)
```
Unless/until the org.Hs.eg.db package got/gets updated, the first result is TUBA4A (A_23_P102109) with Ensembl IDs ENSG00000127824 (gene symbol) and ENSG00000243910 (transcript ID and probe ID). A_23_P102109 appears to be for TUBA4A. And the Ensembl ID for TUBA4A on GeneCards is ENSG00000127824. The other ID appears to be for TUBA4B. And the same goes for e.g. F8A1 - Ensembl ID using gene symbol is for F8A1 but by probe ID it's F8A3. This is an example as to why merging by gene symbols were prioritised. 

Some examples of where Ensembl ID found using probe IDs are not the same as Ensembl IDs found using transcript IDs:
```{r  message=FALSE, warning=FALSE, echo=T}
head( results[ which(!is.na(results$Gene.stable.ID.by.Transcript.ID) & !is.na(results$Gene.stable.ID.by.Probe.ID) & results$Gene.stable.ID.by.Transcript.ID!=results$Gene.stable.ID.by.Probe.ID) ,] ,20)
```
The prioritisation of Ensembl ID by probe ID over Ensembl ID by transcript ID is more debatable. Ensembl IDs listed on GeneCards for PRSS50 and FAM53B are the Ensembl IDs by probe ID. But for KRT31, the ID on GeneCards is the ID by transcript ID. And for OR2H1, we are back to Ensembl ID by probe ID. 

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
results <- results %>% dplyr::mutate("Gene.stable.ID" = NA)
results$Gene.stable.ID <- results$Gene.stable.ID.by.GENE_SYMBOL # Prioritise Ensembl Genes IDs mapped to Gene symbols 
idx <- which(is.na(results$Gene.stable.ID)) 
results$Gene.stable.ID[ idx  ] <- results$Gene.stable.ID.by.Probe.ID[idx] # Next prioritise Ensembl Gene IDs mapped to Probe ID
idx <- which(is.na(results$Gene.stable.ID))
results$Gene.stable.ID[ idx  ] <- results$Gene.stable.ID.by.Transcript.ID[idx] # Finally attempt to map remaining missing Ensembl IDs found using Transcript IDs
rm(idx)
```

## Entrez ID

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
results <- results %>% 
  merge(Ensembl_Entrez, by.x = "Gene.stable.ID", by.y = "Gene.stable.ID", all.x = T)
```

## Save file

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
results <- results %>% dplyr::select(c(ID, GENE_SYMBOL, Gene.stable.ID, EntrezID))
write.csv(results, "Files_Mapping/Probe_Mapping_GSE33479.csv", row.names = F)
```

## GSE114489

We will do the same for GSE11489

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
rm(results.Entrez, results, Ensembl_Probe)
gset <- getGEO("GSE114489", GSEMatrix =TRUE, AnnotGPL=TRUE)
if (length(gset) > 1) idx <- grep("GPL6244", attr(gset, "names")) else idx <- 1
gset <- gset[[idx]]
saveRDS(gset, "Files_data/gset_GSE114489.rds")
fvarLabels(gset) <- make.names(fvarLabels(gset))
gsms <- "000000000000000000000001111111111111112222222223333333333333333"
sml <- c()
for (i in 1:nchar(gsms)) { sml[i] <- substr(gsms,i,i) }
rm(gsms,i)
sml <- paste("G", sml, sep="")
fl <- as.factor(sml)
gset$description <- fl
gs <- factor(sml)
groups <- make.names(c("G0","G1", "G2", "G3"))
levels(gs) <- groups
gset$group <- gs
design <- model.matrix(~group + 0, gset)
colnames(design) <- levels(gs)
fit <- lmFit(gset, design)  # fit linear model
cts <- paste(groups[1], groups[2], sep="-")
cont.matrix <- makeContrasts(contrasts=cts, levels=design)
fit2 <- contrasts.fit(fit, cont.matrix)
fit2 <- eBayes(fit2, 0.01)
tT <- topTable(fit2, adjust="fdr", sort.by="B", number=nrow(fit2))
tT <- subset(tT, select=c("ID","Gene.symbol"))
rm(cont.matrix, design, fit, fit2, gset, cts, fl, groups, gs, idx, sml)

Ensembl_Probe <- read.csv("Files_Ensembl/Ensembl_BioMart_AFFY_HuGene_1_0_st_v1_probe_2021_Dec_104.txt")
Ensembl_Probe <- Ensembl_Probe[-which(duplicated(Ensembl_Probe$AFFY.HuGene.1.0.st.v1.probe)),]

results <- tT
results$Gene.symbol[which(grepl("///", results$Gene.symbol))] <- ""
results <- results %>% 
  merge(Ensembl_Probe, by.x = "ID", by.y = "AFFY.HuGene.1.0.st.v1.probe", all.x = T) %>%
  merge(Ensembl_GeneName, by.x = "Gene.stable.ID", by.y = "Gene.stable.ID", all.x = T) %>%
  merge(Ensembl_Entrez, by.x = "Gene.stable.ID", by.y = "Gene.stable.ID", all.x = T) %>%
  dplyr::mutate(Gene.symbol = ifelse( Gene.symbol=="" & !is.na(Gene.name), Gene.name, Gene.symbol ) ) %>%
  dplyr::mutate(Gene.symbol = ifelse( !is.na(Gene.name) & Gene.symbol!=Gene.name, Gene.name, Gene.symbol ) )
idx <- which(is.na(results$Gene.stable.ID) & results$Gene.symbol!="")
GeneNames.Missing.IDs <- data.frame(ID = results$ID[idx], Gene.symbol = results$Gene.symbol[ idx ]) %>%
  merge(Ensembl_GeneName, by.x = "Gene.symbol", by.y = "Gene.name" ) %>% 
  merge(Ensembl_Entrez, by.x = "Gene.stable.ID", by.y = "Gene.stable.ID", all.x = T)
results <- results[-idx,]
rm(idx)
GeneNames.Missing.IDs <- GeneNames.Missing.IDs %>%
  dplyr::mutate(Gene.name = GeneNames.Missing.IDs$Gene.symbol) %>%
  dplyr::select(Gene.stable.ID, ID, Gene.symbol, Gene.name, EntrezID)
results <- rbind(results, GeneNames.Missing.IDs) %>%
  dplyr::select(-Gene.name)
rm(GeneNames.Missing.IDs)
results <- results[-which(is.na(results$Gene.stable.ID)),]
colnames(results)[which(colnames(results) == "Gene.symbol")] <- "GENE_SYMBOL"
write.csv(results, "Files_Mapping/Probe_Mapping_GSE114489.csv", row.names = F)
rm(results, tT, Ensembl_Probe)
```

## GSE108124

And again for GSE108124

```{r  results='hide', message=FALSE, warning=FALSE, echo=T}
#httr::timeout(300) # default is 60? This isn't working anymore
getOption('timeout')
options(timeout=2000)
gset <- getGEO("GSE108124", GSEMatrix =TRUE, AnnotGPL=FALSE)
if (length(gset) > 1) idx <- grep("GPL18281", attr(gset, "names")) else idx <- 1
#httr::timeout(60) # default is 60? This isn't working anymore
options(timeout=60)
gset <- gset[[idx]]
saveRDS(gset, "Files_data/gset_GSE108124.rds") # download just takes too long, especially for Shiny app later

# make proper column names to match toptable 
fvarLabels(gset) <- make.names(fvarLabels(gset))

# group membership for all samples
gsms <- "000000000000000011111111111111111"
sml <- strsplit(gsms, split="")[[1]]

# assign samples to groups and set up design matrix
gs <- factor(sml)
groups <- make.names(c("g0","g1"))
levels(gs) <- groups
gset$group <- gs
design <- model.matrix(~group + 0, gset)
colnames(design) <- levels(gs)
fit <- lmFit(gset, design)  # fit linear model
# set up contrasts of interest and recalculate model coefficients
cts <- paste(groups[1], groups[2], sep="-")
cont.matrix <- makeContrasts(contrasts=cts, levels=design)
fit2 <- contrasts.fit(fit, cont.matrix)
# compute statistics and table of top significant genes
fit2 <- eBayes(fit2, 0.01)
tT <- topTable(fit2, adjust="fdr", sort.by="B", number=nrow(fit2))
#tT <- subset(tT, select=c("ID","adj.P.Val","P.Value","t","B","logFC"))
tT <- subset(tT, select=c("ID"))
rm(cts, design, groups, gs, gsms, sml, idx, fit, fit2, cont.matrix)

results <- tT %>%
  dplyr::mutate(Gene.stable.ID =  mapIds(org.Hs.eg.db, keys=ID, column="ENSEMBL", keytype="SYMBOL", multiVals="first")) %>%
  merge(Ensembl_GeneName, by.x = "ID", by.y = "Gene.name", all.x = T) # this step finds additional ~100 IDs
results <- results[-which( is.na(results$Gene.stable.ID.x) & is.na(results$Gene.stable.ID.y) ),]
results <- results %>%
  dplyr::mutate(Gene.stable.ID = ifelse( Gene.stable.ID.x==Gene.stable.ID.y, Gene.stable.ID.x, NA))
idx <- which(!is.na(results$Gene.stable.ID.x) & is.na(results$Gene.stable.ID.y))
results$Gene.stable.ID[idx] <- results$Gene.stable.ID.x[idx]
idx <- which(!is.na(results$Gene.stable.ID.y) & is.na(results$Gene.stable.ID.x))
results$Gene.stable.ID[idx] <- results$Gene.stable.ID.y[idx]
idx <- which(results$Gene.stable.ID.x != results$Gene.stable.ID.y)
results$Gene.stable.ID[idx] <- results$Gene.stable.ID.y[idx] # manual inspection, seems like Ensembl BioMart IDs would be better?
rm(idx)
results <- results %>% 
  merge(Ensembl_Entrez, by.x = "Gene.stable.ID", by.y = "Gene.stable.ID", all.x = T)
colnames(results)[which(colnames(results) == "ID")] <- "GENE_SYMBOL"
results <- results %>%
  dplyr::select(Gene.stable.ID, GENE_SYMBOL, EntrezID)

write.csv(results, "Files_Mapping/Probe_Mapping_GSE108124.csv", row.names = F)
rm(gset, tT, results)
```

## GSE109743

For GSE109743, the rownames of the gene expression file are Ensembl IDs. We will start by using org.Hs.eg.db to find gene symbols and Entrez IDs. We prioritised Entrez IDs from org.Hs.eg.db previously but for gene symbols, we prioritised symbols from BioMart. So we will retrieve gene symbols and Entrez IDs, then additional gene symbols from BioMart, then for any differing gene symbols, BioMart will be prioritised. 

```{r results='hide',message=FALSE, warning=FALSE, echo=T}
x <- read.csv("Files_data/GSE109743_exp_count.txt", sep = "\t")
x <- x %>% dplyr::select("X")
colnames(x) <- "Gene.stable.ID"
y <- x %>% 
  dplyr::mutate(GENE_SYMBOL = mapIds(org.Hs.eg.db, keys=Gene.stable.ID, column="SYMBOL", keytype="ENSEMBL", multiVals="first")) %>%
  dplyr::mutate(EntrezID = mapIds(org.Hs.eg.db, keys=Gene.stable.ID, column="ENTREZID", keytype="ENSEMBL", multiVals="first")) %>%
  merge(Ensembl_GeneName, by.x = "Gene.stable.ID", by.y = "Gene.stable.ID", all.x = T) 
idx <- which(y$GENE_SYMBOL != y$Gene.name & !is.na(y$GENE_SYMBOL) & !is.na(y$Gene.name))
```

Here are some examples of where the gene symbols from org.Hs.eg.db and BioMart are not the same:
```{r message=FALSE, warning=FALSE, echo=T}
head( y[idx,] , 15)
```

```{r results='hide',message=FALSE, warning=FALSE, echo=T}
y$GENE_SYMBOL[idx] <- y$Gene.name[idx]
y <- y %>% dplyr::select(-Gene.name) %>%
  dplyr::filter(!is.na(GENE_SYMBOL))
rm(idx)
write.csv(y, "Files_Mapping/Probe_Mapping_GSE109743.csv", row.names = F)
```
